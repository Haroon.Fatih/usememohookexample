import logo from './logo.svg';
import './App.css';
import Usememocomponent from './components/Usememocomponent';

function App() {
  return (
    <div className="App">
      <Usememocomponent />
    </div>
  );
}

export default App;
